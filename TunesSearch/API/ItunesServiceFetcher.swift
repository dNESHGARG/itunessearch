////
////  ItunesServiceFetcher.swift
////  TunesSearch
////
////  Created by Dinesh Garg on 5/4/20.
////  Copyright © 2020 Dinesh Garg. All rights reserved.
////
//
//import Foundation
//import Combine
//
//protocol ItunesMediaFetchable {
//    func fetchSearchResults(_ query: String) -> AnyPublisher<ItunesSearchResponse, ItunesError>
//}
//
//
//class ItunesServiceFetcher {
//    private let session: URLSession
//
//    init(session: URLSession = .shared) {
//        self.session = session
//    }
//}
//
//extension ItunesServiceFetcher: ItunesMediaFetchable {
//    func fetchSearchResults(_ query: String) -> AnyPublisher<ItunesSearchResponse, ItunesError> {
//        searchResults(with: makeAllMediaSearchComponents(withSearchItem: query))
//    }
//
//    private func searchResults<T>(
//        with components: URLComponents
//    ) -> AnyPublisher<T, ItunesError> where T: Decodable {
////        guard let url = components.url else {
////            let error = ItunesError.network(description: "Couldn't create URL")
////            return Fail(error: error).eraseToAnyPublisher()
////        }
//        let url = URL(string: "https://itunes.apple.com/search?term=Suits&media=all")!
//        return session.dataTaskPublisher(for: URLRequest(url: url))
//            .mapError { error in
//                .parsing(description: error.localizedDescription)
//        }
//        .flatMap(maxPublishers: .max(1)) { pair in
//            decode(pair.data)
//        }
//        .eraseToAnyPublisher()
//    }
//}
//
//// MARK: - OpenWeatherMap API
//private extension ItunesServiceFetcher {
//    struct ItunesAPI {
//        static let scheme = "https"
//        static let host = "itunes.apple.com"
//    }
//
//    enum Path: String {
//        case search = "search"
//        case lookup = "lookup"
//    }
//
//    //"https://itunes.apple.com/search?term=Suits&media=all"
//    func makeAllMediaSearchComponents(
//        withSearchItem search: String
//    ) -> URLComponents {
//        var components = URLComponents()
//        components.scheme = ItunesAPI.scheme
//        components.host = ItunesAPI.host
//        components.path = Path.search.rawValue
//
//        components.queryItems = [
//            URLQueryItem(name: "term", value: search),
//            URLQueryItem(name: "media", value: "all"),
//        ]
//
//        return components
//    }
//}
