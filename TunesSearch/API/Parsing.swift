//
//  Parsing.swift
//  TunesSearch
//
//  Created by Dinesh Garg on 5/4/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

import Foundation
import Combine

func decode<T: Decodable>(_ data: Data) -> AnyPublisher<T, ItunesError> {
  let decoder = JSONDecoder()
  //decoder.dateDecodingStrategy = .secondsSince1970

  return Just(data)
    .decode(type: T.self, decoder: decoder)
    .mapError { error in
      .parsing(description: error.localizedDescription)
    }
    .eraseToAnyPublisher()
}

enum ItunesError: Error {
        case unknown
        case invalidSearchTerm
        case invalidURL
        case invalidServerResponse
        case serverError(Int)
        case missingData
        case invalidJSON(Swift.Error)
        case parsing(description: String)
        case network(description: String)
}
