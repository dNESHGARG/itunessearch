//
//  ItunesAPI.swift
//  TunesSearch
//
//  Created by Dinesh Garg on 5/7/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import UIKit
import Combine

enum ItunesAPI {
    private static let imageBase =  URL(string: "https://www.apple.com")!
    private static let base =  URL(string: "https://www.apple.com")!
    private static let agent = Agent()
    
    static func search(forMedia query: String) -> AnyPublisher<ItunesSearchResponse<ItunesMedia>, Error> {
       var components = URLComponents(url: base.appendingPathComponent("/search"), resolvingAgainstBaseURL: true)
        components?.queryItems = [
                  URLQueryItem(name: "term", value: query),
                  URLQueryItem(name: "media", value: "all"),
              ]
        let request = components?.request
        
        return agent.run(request!)
    }
}

private extension URLComponents {
    var request: URLRequest? {
        url.map { URLRequest.init(url: $0) }
    }
}
