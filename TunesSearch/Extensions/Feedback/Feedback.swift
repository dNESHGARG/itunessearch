//
//  Feedback.swift
//  TunesSearch
//
//  Created by Dinesh Garg on 5/7/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import UIKit
import Combine

struct Feedback <State, Event> {
    let run: (AnyPublisher<State, Never>) -> AnyPublisher<Event, Never>
}

extension Feedback {
    init<Effect: Publisher>(effects: @escaping (State) -> Effect) where Effect.Output == Event, Effect.Failure == Never {
        self.run = { state -> AnyPublisher<Event, Never> in
            state
                .map { effects($0) }
                .switchToLatest()
                .eraseToAnyPublisher()
        }
    }
}
