//
//  View+Extension.swift
//  TunesSearch
//
//  Created by Dinesh Garg on 5/7/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

extension View {
    func eraseToAnyView() -> AnyView { AnyView(self) }
}
