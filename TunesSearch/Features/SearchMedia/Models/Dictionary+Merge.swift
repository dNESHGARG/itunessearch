//
//  Dictionary+Merge.swift
//  TunesSearch
//
//  Created by Dinesh Garg on 5/4/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

extension Dictionary {
    mutating func unionInPlace(_ dictionary: [Key: Value]) {
    for (key, value) in dictionary {
      self[key] = value
    }
  }

  func union(_ dictionary: [Key: Value]) -> [Key: Value] {
    var dict = self
    dict.unionInPlace(dictionary)
    return dict
  }
}
