//
//  Media.swift
//  TunesSearch
//
//  Created by Dinesh Garg on 5/4/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

public enum Media {
  case movie(Entity?)
  case podcast(Entity?)
  case music(Entity?)
  case musicVideo(Entity?)
  case audioBook(Entity?)
  case shortFilm(Entity?)
  case tvShow(Entity?)
  case software(Entity?)
  case eBook(Entity?)
  case all(Entity?)

  fileprivate var value: String {
    switch self {
    case .movie:
      return "movie"
    case .podcast:
      return "podcast"
    case .music:
      return "music"
    case .musicVideo:
      return "musicvideo"
    case .audioBook:
      return "audiobook"
    case .shortFilm:
      return "shortFilm"
    case .tvShow:
      return "tvShow"
    case .software:
      return "software"
    case .eBook:
      return "ebook"
    case .all:
      return "all"
    }
  }

  fileprivate var entity: Entity? {
    switch self {
    case .movie(let entity):
      return entity
    case .podcast(let entity):
      return entity
    case .music(let entity):
      return entity
    case .musicVideo(let entity):
      return entity
    case .audioBook(let entity):
      return entity
    case .shortFilm(let entity):
      return entity
    case .tvShow(let entity):
      return entity
    case .software(let entity):
      return entity
    case .eBook(let entity):
      return entity
    case .all(let entity):
      return entity
    }
  }
}

extension Media {

  var parameters: [String: String] {

    var media = ["media": value]

    if let entity = entity?.parameter {
      media.unionInPlace(entity)
    }

    return media
  }
}
