//
//  EntityType.swift
//  TunesSearch
//
//  Created by Dinesh Garg on 5/4/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

public protocol EntityType {
  var value: String { get }
  var parameter: [String: String] { get }
}

public extension EntityType {
  var parameter: [String: String] {
    return ["entity": value]
  }
}
