//
//  MediaModel.swift
//  TunesSearch
//
//  Created by Dinesh Garg on 5/4/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

struct ItunesSearchResponse<T: Codable>: Codable {
    var resultCount: Int
    var results: [T]
}

struct ItunesMedia: Codable {
    var wrappedType: String
    var kind: String
    var artistId: Int
    var artistName: String
    var collectionName: String
    var trackname: String
    var collectionCensoredName: String?
    var trackCensoredName: String?
    var artistViewUrl: String?
    var collectionViewUrl: String?
    var trackViewUrl: String?
    var collectionExplicitness: String?
    var trackExplicitness: String?
    var artworkUrl100: String?
    var artworkUrl60: String?
    var previewUrl: String?
    var trackTimeMillis: String?
    var collectionPrice: Int?
    var trackPrice: Int?
}

extension ItunesSearchResponse: Mockable {
    var mock:ItunesSearchResponse {
        return ItunesSearchResponse<ItunesMedia>(resultCount: 50, results: [ItunesMedia]()) as! ItunesSearchResponse<T>
    }
}

protocol Mockable {
    var mock: Self { get }
}

