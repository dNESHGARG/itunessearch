//
//  SearchViewModel.swift
//  TunesSearch
//
//  Created by Dinesh Garg on 5/3/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import Combine

final class SearchViewModel: ObservableObject {
    @Published private(set) var state = State.idle
    
    private var bag = Set<AnyCancellable>()
    private let input = PassthroughSubject<Event, Never>()
    
    @Published var searchTerm: String = ""
    
    init() {
        Publishers.system(
            initial: state,
            reduce: Self.reduce,
            scheduler: RunLoop.main,
            feedbacks: [
                Self.loadMedia()
                //Self.userInput(input: input.eraseToAnyPublisher())
            ]
        )
        .assign(to: \.state, on: self)
        .store(in: &bag)
    }
    
    deinit {
        bag.removeAll()
    }
    
    func send(event: Event) {
        input.send(event)
    }
}

extension SearchViewModel {
    enum State {
        case idle(String)
        case loading(String)
        case loaded([MediaItem])
        case error(Error)
    }
    
    enum Event {
        case onAppear
        case onMediaLoaded([MediaItem])
        case onFailedToLoadMedia(Error)
    }
    
    struct MediaItem: Identifiable {
        let id: Int
        let title: String
        let poster: String?
        
        init(media: ItunesMedia) {
            id = media.artistId
            title = media.artistName
            poster = media.artworkUrl100
        }
    }
}

extension SearchViewModel {
    static func reduce(_ state: State, _ event: Event) -> State {
        switch state {
        case .idle(let query):
            switch event {
            case .onAppear:
                return .loading(query)
            default:
                return state
            }
        case .loading:
            switch event {
            case .onFailedToLoadMedia(let error):
                return .error(error)
            case .onMediaLoaded(let movies):
                return .loaded(movies)
            default:
                return state
            }
        case .error:
            return state
        case .loaded:
            return state
        }
    }
}

extension SearchViewModel {
    static func loadMedia() -> Feedback<State, Event> {
        Feedback { (state: State) -> AnyPublisher<Event, Never> in
            guard case .loading(let query) = state else {
                return Empty().eraseToAnyPublisher()
            }
            
            return ItunesAPI.search(forMedia: query)
                .map{ $0.results.map(MediaItem.init) }
                .map(Event.onMediaLoaded)
                .catch { Just(Event.onFailedToLoadMedia($0)) }
                .eraseToAnyPublisher()
            
        }
    }
    
    static func userInput(input: AnyPublisher<Event, Never>) -> Feedback<State, Event> {
          Feedback { _ in input }
      }
}
//class SearchViewModel: ObservableObject, Identifiable {
//
//    var fetcher = ItunesServiceFetcher()
//
//    @Published var searchTerm: String = ""
//
//    private var disposables = Set<AnyCancellable>()
//
//    @Published var dataSource: [ItunesMedia] = [ItunesMedia]()
//
//    init(
//        fetcher: ItunesServiceFetcher = ItunesServiceFetcher(),
//        scheduler: DispatchQueue = DispatchQueue(label: "SearchViewModel")
//    ) {
//        self.fetcher = fetcher
//        $searchTerm
//            .dropFirst(1)
//            .debounce(for: .seconds(0.5), scheduler: scheduler)
//            .sink(receiveValue: fetchSearchResults(forQuery:))
//            .store(in: &disposables)
//    }
//
//    func fetchSearchResults(forQuery query: String)  {
//        print("searching")
//        let q = "War"
//        fetcher.fetchSearchResults(q)
//            .map { response in
//                print(response)
//                //response.list.map(ItunesSearchResponse())
//        }
//        //.map(Array.removeDuplicates)
//        .receive(on: DispatchQueue.main)
//        .sink(
//            receiveCompletion: { [weak self] value in
//                guard let self = self else { return }
//                switch value {
//                case .failure:
//                    self.dataSource = []
//                case .finished:
//                    break
//                }
//            },
//            receiveValue: { [weak self] result in
//                guard let self = self else { return }
//                print(result)
//                //self.dataSource = result
//        })
//            .store(in: &disposables)
//    }
//
//}

/*
 ViewModel: ObservableObjects {
 var bag = Set<AnyCancellable>()
 var input = Passthrough<Event, failure>
 var state =  State.idle
 @Published var searchTerm
 
 enum State {}
 enum Event {}
 
 
 }
 */
