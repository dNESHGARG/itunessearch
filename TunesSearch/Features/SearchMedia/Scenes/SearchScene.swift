//
//  SearchScene.swift
//  TunesSearch
//
//  Created by Dinesh Garg on 5/3/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct SearchScene: View {
    @ObservedObject var viewModel: SearchViewModel
    
    init() {
        self.viewModel = SearchViewModel()
    }
    
    @State private var searchTerm: String = ""
    
    var body: some View {
        NavigationView {
            List {
                SearchBar(text: $viewModel.searchTerm, onTextChanged: searchMedia)
                
//                if viewModel.dataSource.isEmpty {
//                    emptySection
//                } else {
//                    searchList
//                }
                
            }
                
            .navigationBarTitle(Text("Search Bar"))
        }
    }
}

private extension SearchScene {

    var searchList: some View {
//      Section {
////        ForEach(viewModel.dataSource, content: searchRow)
//
//      }
        Text("Some Text")
    }
    
    var emptySection: some View {
        Section {
            Text("No results")
                .foregroundColor(.gray)
        }
    }
    
    var searchRow: some View {
         Section {
             Text("No results")
                 .foregroundColor(.white)
         }
     }
}

extension SearchScene {
    func searchMedia(for searchText: String) {
          if !searchText.isEmpty {
            print("text is -\(searchText)")
              //model.getMovieSearchResults(for: searchText)
          }
      }
}

struct SearchScene_Previews: PreviewProvider {
    static var previews: some View {
        SearchScene()
    }
}

